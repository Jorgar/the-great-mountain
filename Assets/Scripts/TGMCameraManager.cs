﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TGMCameraManager : MonoBehaviour {


    Transform trObject, trCamera;
    public float distanceX, distanceY;

	// Use this for initialization
	void Start () {
        trObject = GameObject.Find("player").GetComponent<Transform>();
        trCamera = this.gameObject.GetComponent<Transform>();

    }
	
	// Update is called once per frame
	void Update () {
        trCamera.position = new Vector3(trObject.position.x + distanceX, trObject.position.y + distanceY, trCamera.position.z);
	}
}
