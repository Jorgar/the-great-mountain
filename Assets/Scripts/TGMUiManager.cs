﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TGMUiManager : MonoBehaviour
{

    private bool isInventoryActivated;
    private bool isOptionsActivated;
    private bool isUIActivated;
    private static bool isTutorialActive = true; //default true

    //Survivor System
    private Slider foodBar;
    private Slider hidratationBar;
    private Slider sleepBar;

    //Inventory System
    private int actualInventoryItemIndex;

    private TGMCharacterManager characterManager;
    private TGMInventoryManager inventoryManager;
    private GooglePlayManager gPManager;

    //Getters and Setters
    public bool IsUIActivated { get { return isUIActivated; } set { isUIActivated = value; } }
    public bool IsOptionsActivated { get { return isOptionsActivated; } set { isOptionsActivated = value; } }
    public bool IsInventoryActivated { get { return isInventoryActivated; } set { isInventoryActivated = value; } }
    public int ActualInventoryItemIndex { get { return actualInventoryItemIndex; } set { actualInventoryItemIndex = value; } }
    public Slider FoodBar{get{return foodBar;}set {foodBar = value;}}
    public Slider HidratationBar{get{return hidratationBar;}set{hidratationBar = value; }}
    public Slider SleepBar{get{ return sleepBar;} set {  sleepBar = value;}}

    void Awake()
    {
        IsUIActivated = false;
        IsInventoryActivated = false;
        IsOptionsActivated = false;
        characterManager = GameObject.FindGameObjectWithTag("Player").GetComponent<TGMCharacterManager>();
        gPManager = GameObject.Find("_Manager").GetComponent<GooglePlayManager>();

    }

    void Start()
    {
        if (isTutorialActive)
        {
            StartCoroutine(ActionTutorial(true)); //show tutorial
        }

        FoodBar = GameObject.Find("StadisticsBars").transform.Find("foodBar").gameObject.GetComponent<Slider>();
        HidratationBar = GameObject.Find("StadisticsBars").transform.Find("hidratationBar").gameObject.GetComponent<Slider>();
        SleepBar = GameObject.Find("StadisticsBars").transform.Find("sleepBar").gameObject.GetComponent<Slider>();

    }

    private void UIButtonPressed(int idButton)
    {
        if (characterManager.State.ToString().Equals("walking_normal") || characterManager.State.ToString().Equals("walking_effort"))
            isUIActivated = true;
        GameObject.FindGameObjectWithTag("Player").GetComponent<TGMCharacterManager>().stopCharacter();
        if (GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("walking_normal"))
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>().SetBool("walking", false);
        }
        switch (idButton)
        {
            case Constants.INVENTORY_PRESS:
                if (characterManager.State.ToString().Equals("walking_normal") || characterManager.State.ToString().Equals("walking_effort"))
                {
                    if (GameObject.Find("Canvas").transform.Find("Inventory").transform.Find("subButtons_menu").gameObject.activeSelf)
                        StartCoroutine(ActionSubMenuItem());
                    GameObject optionsButton = GameObject.Find("Canvas").transform.Find("Options_Button").gameObject;
                    if (isInventoryActivated)
                    {
                        isInventoryActivated = false;
                        optionsButton.SetActive(true);
                    }
                    else
                    {
                        isInventoryActivated = true;
                        optionsButton.SetActive(false);
                    }
                    StartCoroutine(ActionInventory(IsInventoryActivated));
                }
                break;
            case Constants.ITEM_PRESS:
                GameObject buttonsMenu = GameObject.Find("Inventory").transform.Find("subButtons_menu").gameObject;
                inventoryManager = GameObject.Find("_Manager").GetComponent<TGMInventoryManager>();
                Image useButtonImage = buttonsMenu.transform.Find("use_button").gameObject.GetComponent<Image>();

                if (!buttonsMenu.activeSelf)
                {
                    buttonsMenu.SetActive(true);
                    //Change the subMenu image button acord to the item type. 
                    if (inventoryManager.Items[actualInventoryItemIndex].GetType() == typeof(Food))
                    {
                        useButtonImage.sprite = Resources.Load("Sprites/UI_elements/button_Eat", typeof(Sprite)) as Sprite;
                    }
                    else if (inventoryManager.Items[actualInventoryItemIndex].GetType() == typeof(Tool))
                    {
                        Tool auxTool = (Tool)inventoryManager.Items[actualInventoryItemIndex];
                        if (auxTool.Utility.Equals("climbing"))
                        {
                            useButtonImage.sprite = Resources.Load("Sprites/UI_elements/button_use", typeof(Sprite)) as Sprite;
                        }
                        else
                        {
                            useButtonImage.sprite = Resources.Load("Sprites/UI_elements/button_Build", typeof(Sprite)) as Sprite;
                        }
                    }
                    buttonsMenu.GetComponent<RectTransform>().position = inventoryManager.Slots[actualInventoryItemIndex].GetComponent<RectTransform>().position;
                }
                else
                {
                    StartCoroutine(ActionSubMenuItem());
                }
                break;
            case Constants.ITEM_INFO_PRESS:
                GameObject infoMenu = GameObject.Find("Canvas").transform.Find("Info").gameObject;
                TGMInventoryManager inventoryInfo = GameObject.Find("_Manager").GetComponent<TGMInventoryManager>();
                TextMeshProUGUI title = infoMenu.transform.Find("title_item").GetComponent<TextMeshProUGUI>();
                TextMeshProUGUI description = infoMenu.transform.Find("text_item").GetComponent<TextMeshProUGUI>();
                Image itemImageInfo = infoMenu.transform.Find("item_image").gameObject.GetComponent<Image>();
                title.text = inventoryInfo.Items[actualInventoryItemIndex].Name;
                description.text = inventoryInfo.Items[actualInventoryItemIndex].Description;
                itemImageInfo.sprite = Resources.Load("Sprites/Inventory_elements/" + inventoryInfo.Items[actualInventoryItemIndex].Image, typeof(Sprite)) as Sprite;
                StartCoroutine(ActionSubMenuItem());
                infoMenu.SetActive(true);
                break;
            case Constants.ITEM_REMOVE_PRESS:
                StartCoroutine(ActionSubMenuItem());
                GameObject.Find("Canvas").transform.Find("Emergent_Window_Delete_Item").gameObject.SetActive(true);
                break;
            case Constants.ITEM_REMOVE_PRESS_YES:
                inventoryManager.Items.RemoveAt(actualInventoryItemIndex);
                List<Item> currentItems = new List<Item>();
                foreach (Item item in inventoryManager.Items)
                {
                    if (item != null)
                        currentItems.Add(item);
                }
                inventoryManager.ClearInventory();
                inventoryManager.InitInventory();
                foreach (Item item in currentItems)
                    inventoryManager.AddItem(item.ID);
                StartCoroutine(ActionEmergentWindowDeleteItem());
                break;
            case Constants.ITEM_REMOVE_PRESS_NO:
                StartCoroutine(ActionEmergentWindowDeleteItem());
                break;
            case Constants.ITEM_USE_PRESS:
                if (inventoryManager.Items[actualInventoryItemIndex].GetType() == typeof(Food))
                {
                    StartCoroutine(ActionSubMenuItem());
                    Food auxFood = (Food)inventoryManager.Items[actualInventoryItemIndex];
                    characterManager.AddCalories(auxFood.Calories);
                    characterManager.AddSleep(auxFood.Sleep);
                    characterManager.AddHidratation(auxFood.Hidratation);
                    auxFood.Uses -= 1;
                    inventoryManager.Items[actualInventoryItemIndex] = auxFood;
                    if(auxFood.Uses == 0)
                    {
                        inventoryManager.Items.RemoveAt(actualInventoryItemIndex);
                        List<Item> currentItemsUsed = new List<Item>();
                        foreach (Item item in inventoryManager.Items)
                        {
                            if (item != null)
                                currentItemsUsed.Add(item);
                        }
                        inventoryManager.ClearInventory();
                        inventoryManager.InitInventory();
                        foreach (Item item in currentItemsUsed)
                            inventoryManager.AddItem(item.ID);
                    }
                }else if(inventoryManager.Items[actualInventoryItemIndex].GetType() == typeof(Tool))
                {
                    Tool auxTool = (Tool)inventoryManager.Items[actualInventoryItemIndex];
                    if (auxTool.Utility.Equals(Constants.ITEM_BUILDING))
                    {
                        if (characterManager.InsideBuildingZone)
                        {
                            Tool auxToolBuilding = (Tool)inventoryManager.Items[actualInventoryItemIndex];
                            GameObject itemBuild = Instantiate(Resources.Load("Prefabs/items/" + auxToolBuilding.PrefabName) as GameObject);
                            itemBuild.transform.position = GameObject.FindGameObjectWithTag("Player").transform.position;
                            itemBuild.name = auxToolBuilding.ID;
                            //itemBuild.transform.SetParent(GameObject.Find("World_tools").transform, false);
                            //Delete object from inventory
                            inventoryManager.Items.RemoveAt(actualInventoryItemIndex);
                            List<Item> currentItemsUsed = new List<Item>();
                            foreach (Item item in inventoryManager.Items)
                            {
                                if (item != null)
                                    currentItemsUsed.Add(item);
                            }
                            inventoryManager.ClearInventory();
                            inventoryManager.InitInventory();
                            foreach (Item item in currentItemsUsed)
                                inventoryManager.AddItem(item.ID);

                            StartCoroutine(ActionSubMenuItem());
                            StartCoroutine(ActionInventory(false));
                        }
                    }
                    else if (auxTool.Utility.Equals(Constants.ITEM_CLIMBING))
                    {
                        if (characterManager.InsideClimbing)
                        {
                            StartCoroutine(ActionSubMenuItem());
                            StartCoroutine(ActionInventory(false));
                            characterManager.Climb();
                        }
                    }
                    else if (auxTool.Utility.Equals(Constants.ITEM_LIGHTNING))
                    {
                        //TO DO
                        //Active light if day-night system
                    }
                }
         
                break;
            case Constants.INFO_CLOSE_PRESS:
                StartCoroutine(ActionInfoItem());
                break;
            case Constants.OPTIONS_PRESS:
                IsOptionsActivated = true;
                StartCoroutine(ActionOptions(true));
                break;
            case Constants.RESUME_PRESS:
                IsOptionsActivated = false;
                StartCoroutine(ActionOptions(false));
                break;
            case Constants.SOUND_PRESS:
                GameObject soundButton = GameObject.Find("Canvas").transform.Find("Pause").transform.Find("Button_Sound").gameObject;
                if (!soundButton.GetComponent<Image>().sprite.Equals(Resources.Load<Sprite>("Sprites/UI_elements/icon_sound_pressed")))
                {
                    soundButton.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI_elements/icon_sound_pressed");
                    GameObject.Find("_MusicManager").GetComponent<TGMMusicManager>().IsSound = false;
                }
                else
                {
                    soundButton.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UI_elements/icon_sound");
                    GameObject.Find("_MusicManager").GetComponent<TGMMusicManager>().IsSound = true;
                }
                break;
            case Constants.EXIT_PRESS:
                StartCoroutine(ActionEmergentWindow(true));
                break;
            case Constants.EXIT_YES_PRESS:
                IsOptionsActivated = false;
                IsUIActivated = false;
                SceneManager.LoadScene("main_menu", LoadSceneMode.Single);
                break;
            case Constants.EXIT_NO_PRESS:
                StartCoroutine(ActionEmergentWindow(false));
                break;
            case Constants.EXIT_CLOSE_PRESS:
                StartCoroutine(ActionEmergentWindow(false));
                break;
            case Constants.GAME_WIN:
                GameObject winMenu = GameObject.Find("Canvas").transform.Find("win_window").gameObject;
                GameObject optionsBu = GameObject.Find("Canvas").transform.Find("Options_Button").gameObject;
                //gPManager.OnAddScoreToLeaderBoard();
                optionsBu.SetActive(false);
                winMenu.SetActive(true);
                Time.timeScale = 0;
                break;
            case Constants.GAME_LOST:
                GameObject optionsB = GameObject.Find("Canvas").transform.Find("Options_Button").gameObject;
                GameObject gaOvMenu = GameObject.Find("Canvas").transform.Find("gameover_window").gameObject;
                optionsB.SetActive(false);
                gaOvMenu.SetActive(true);
                Time.timeScale = 0;
                break;
            default:
                break;
        }
    }

    //Receivers for Unity Buttons. 
    public void InventoryTouch() { UIButtonPressed(Constants.INVENTORY_PRESS); }
    public void ItemTouch() { UIButtonPressed(Constants.ITEM_PRESS); }
    public void ItemInfoTouch() { UIButtonPressed(Constants.ITEM_INFO_PRESS); }
    public void ItemUseTouch() { UIButtonPressed(Constants.ITEM_USE_PRESS); }
    public void ItemRemoveTouch() { UIButtonPressed(Constants.ITEM_REMOVE_PRESS); }
    public void ItemRemoveYesTouch() { UIButtonPressed(Constants.ITEM_REMOVE_PRESS_YES); }
    public void ItemRemoveNoTouch() { UIButtonPressed(Constants.ITEM_REMOVE_PRESS_NO); }
    public void InfoCloseTouch() { UIButtonPressed(Constants.INFO_CLOSE_PRESS); }
    public void OptionsTouch() { UIButtonPressed(Constants.OPTIONS_PRESS); }
    public void ResumeTouch() { UIButtonPressed(Constants.RESUME_PRESS); }
    public void SoundTouch() { UIButtonPressed(Constants.SOUND_PRESS); }
    public void ExitTouch() { UIButtonPressed(Constants.EXIT_PRESS); }
    public void ExitYesTouch() { UIButtonPressed(Constants.EXIT_YES_PRESS); }
    public void ExitNoTouch() { UIButtonPressed(Constants.EXIT_NO_PRESS); }
    public void ExitCloseTouch() { UIButtonPressed(Constants.EXIT_CLOSE_PRESS);}
    public void WinTouch() { UIButtonPressed(Constants.GAME_WIN); }
    public void GameOverTouch() { UIButtonPressed(Constants.GAME_LOST); }


    public IEnumerator ActionInventory(bool state)
    {
        GameObject inventory = GameObject.Find("Canvas").transform.Find("Inventory").gameObject;
        if (state) //open
        {
            inventory.SetActive(true);
            IsUIActivated = true;
        }
        else //close
        {
            Animator anim = inventory.GetComponent<Animator>();
            anim.SetTrigger("UI_Inventory_close");
            yield return new WaitForSeconds(0.25f);
            inventory.SetActive(false);
            IsUIActivated = false;
            GameObject optionsButton = GameObject.Find("Canvas").transform.Find("Options_Button").gameObject;
            optionsButton.SetActive(true);
        }
        yield return null;
    }


    IEnumerator ActionOptions(bool state) //A.K.A Pause menu
    {
        GameObject pauseMenu = GameObject.Find("Canvas").transform.Find("Pause").gameObject;
        if (state) //open
        {
            pauseMenu.SetActive(true);
            IsUIActivated = true;
            GameObject optionsButton = GameObject.Find("Canvas").transform.Find("Options_Button").gameObject;
            optionsButton.SetActive(false);
        }
        else //close
        {
            Animator anim = pauseMenu.GetComponent<Animator>();
            anim.SetTrigger("UI_Pause_Close");
            GameObject optionsButton = GameObject.Find("Canvas").transform.Find("Options_Button").gameObject;
            optionsButton.SetActive(true);
            yield return new WaitForSeconds(0.25f);
            pauseMenu.SetActive(false);
            IsUIActivated = false;
        }
        yield return null;
    }


    IEnumerator ActionEmergentWindow(bool state)
    {
        GameObject emergentWindow = GameObject.Find("Canvas").transform.Find("Emergent_Window_Exit").gameObject;
        if (state) //open
        {
            emergentWindow.SetActive(true);
        }
        else //close
        {
            Animator anim = emergentWindow.GetComponent<Animator>();
            anim.SetTrigger("UI_Emergent_Window_Close");
            yield return new WaitForSeconds(0.25f);
            emergentWindow.SetActive(false);
        }
        yield return null;
    }


    IEnumerator ActionSubMenuItem()
    {
        //close
        GameObject subMenuItem = GameObject.Find("Inventory").transform.Find("subButtons_menu").gameObject;
        subMenuItem.GetComponent<Animator>().SetTrigger("UI_Inventory_Submenu_close");
        yield return new WaitForSeconds(0.25f);
        subMenuItem.SetActive(false);
    }


    IEnumerator ActionInfoItem()
    {
        //close
        GameObject infoMenu = GameObject.Find("Canvas").transform.Find("Info").gameObject;
        infoMenu.GetComponent<Animator>().SetTrigger("UI_Info_window_close");
        yield return new WaitForSeconds(0.25f);
        infoMenu.SetActive(false);
    }

    IEnumerator ActionEmergentWindowDeleteItem()
    {
        GameObject emergentWindowDeleteItem = GameObject.Find("Canvas").transform.Find("Emergent_Window_Delete_Item").gameObject;
        Animator anim = emergentWindowDeleteItem.GetComponent<Animator>();
        anim.SetTrigger("UI_Emergent_Window_delete_close");
        yield return new WaitForSeconds(0.25f);
        emergentWindowDeleteItem.SetActive(false);
    }

    IEnumerator ActionTutorial(bool ini)
    {
        if (ini) //show
        {
            IsUIActivated = true;
            yield return new WaitForSeconds(2f);
            GameObject.Find("Canvas").transform.Find("Tutorial").gameObject.SetActive(true);
        }
        else //close
        {
            GameObject.Find("Canvas").transform.Find("Tutorial").gameObject.GetComponent<Animator>().SetTrigger("close");
            yield return new WaitForSeconds(0.25f);
            GameObject.Find("Canvas").transform.Find("Tutorial").gameObject.SetActive(false);
            IsUIActivated = false;
            isTutorialActive = false;
        }
    }

    public void closeTutorial()
    {
        StartCoroutine(ActionTutorial(false));
    }
}
