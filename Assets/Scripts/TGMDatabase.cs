﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System.IO;

public class TGMDatabase : MonoBehaviour {

    private List<Item> database = new List<Item>();
    private JsonData foodData;
    private JsonData toolsData;

    void Start()
    {
        readFoodJson();
        readToolsJson();
        InitDatabase();
    }

    public void readFoodJson()
    {
        string filePath = Application.streamingAssetsPath + "/Database/db_food.json";
        string jsonString;

        if (Application.platform == RuntimePlatform.Android)
        {
            WWW reader = new WWW(filePath);
            while (!reader.isDone) { }

            jsonString = reader.text;
        }
        else
        {
            jsonString = File.ReadAllText(filePath);
        }


        foodData = JsonMapper.ToObject(jsonString);
    }

    public void readToolsJson()
    {
        string filePath = Application.streamingAssetsPath + "/Database/db_tools.json";
        string jsonString;

        if (Application.platform == RuntimePlatform.Android)
        {
            WWW reader = new WWW(filePath);
            while (!reader.isDone) { }

            jsonString = reader.text;
        }
        else
        {
            jsonString = File.ReadAllText(filePath);
        }


        toolsData = JsonMapper.ToObject(jsonString);
    }

    public Item GetItem(string id)
    {
        for(int i = 0; i<database.Count; i++)
        {
            if(database[i].ID.Equals(id))
            {
                return database[i];
            }
        }
        return null;
    }

    void InitDatabase()
    {
        //Food
        for(int i = 0; i < foodData.Count; i++)
        {
            database.Add(new Food(foodData[i]["id"].ToString(), foodData[i]["name"].ToString(),foodData[i]["description"].ToString(),
            foodData[i]["image"].ToString(), (int)foodData[i]["calories"], (int)foodData[i]["uses"], (int)foodData[i]["hidratation"],(int)foodData[i]["sleep"]));
        }

        //Tools
        for (int i = 0; i < toolsData.Count; i++)
        {
            database.Add(new Tool(toolsData[i]["id"].ToString(), toolsData[i]["name"].ToString(), toolsData[i]["description"].ToString(),
            toolsData[i]["image"].ToString(), toolsData[i]["utility"].ToString(), toolsData[i]["prefab_name"].ToString(),(bool)toolsData[i]["instance"]));
        }
    }
}

public class Item
{
    public string ID { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public string Image { get; set; }
    

    public Item(string id, string name, string description, string image)
    {
        this.ID = id;
        this.Name = name;
        this.Description = description;
        this.Image = image;
    }

    public Item()
    {
        ID = "-1";
    }
}

public class Food : Item
{
    public int Calories { get; set; }
    public int Uses { get; set; }
    public int Hidratation { get; set; }
    public int Sleep { get; set; }

    public Food(string id, string name, string description, string image, int calories, int uses, int hidratation, int sleep): base(id, name, description, image)
    {
        this.Calories = calories;
        this.Hidratation = hidratation;
        this.Sleep = sleep;
        this.Uses = uses;
    }
}

public class Tool : Item
{
    public string Utility { get; set; }
    public string PrefabName { get; set; }
    public bool Instance { get; set; }

    public Tool(string id, string name, string description, string image, string utility, string prefabName, bool instance) : base(id, name, description, image)
    {
        this.Utility = utility;
        this.PrefabName = prefabName;
        this.Instance = instance;
    }
}
