﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TGMInventoryManager : MonoBehaviour {
    GameObject inventoryPanel;
    GameObject slotPanel;
    GameObject inventorySlot;
    GameObject inventoryItem;
    TGMDatabase database;

    GameObject buttonsMenu;

    private List<Item> items = new List<Item>();
    private List<GameObject> slots = new List<GameObject>();
    private int maxSizeInventory = 9; 

    public List<Item> Items{get{return items;}set{items = value;}}
    public List<GameObject> Slots{get{return slots;} set{slots = value;}}

    //Getters and setters


    void Start()
    {
        inventoryPanel = GameObject.Find("Canvas").transform.Find("Inventory").gameObject;
        slotPanel = GameObject.Find("Canvas").transform.Find("Inventory").transform.Find("Content").gameObject;
        buttonsMenu = GameObject.Find("Canvas").transform.Find("Inventory").transform.Find("subButtons_menu").gameObject;
        inventorySlot = Resources.Load("Prefabs/slot") as GameObject;
        inventoryItem = Resources.Load("Prefabs/item") as GameObject;
        database = GameObject.Find("_Manager").GetComponent<TGMDatabase>();

        InitInventory();

        AddItem("0t");
        AddItem("2t");
        AddItem("0f");
        AddItem("1f");
        AddItem("2f");
        AddItem("3f");
    }

    public void InitInventory()
    {
        for (int i = 0; i < maxSizeInventory; i++)
        {
            Items.Add(null);
            Slots.Add(Instantiate(inventorySlot));
            Slots[i].transform.SetParent(slotPanel.transform, false);
        }
    }

    public void AddItem(string id)
    {
        Item itemToAdd = database.GetItem(id);

        for(int i = 0; i<maxSizeInventory; i++)
        {
            if(Items[i] == null)
            {
                Items[i] = itemToAdd;
                GameObject itemObj = Instantiate(inventoryItem);
                itemObj.transform.SetParent(Slots[i].transform, false);
                itemObj.name = i.ToString();
                itemObj.GetComponent<Image>().sprite = Resources.Load("Sprites/Inventory_elements/" + Items[i].Image, typeof(Sprite)) as Sprite;
                break;
            }
        }
    }

    public void ClearInventory()
    {
        foreach (Transform child in slotPanel.transform)
        {
            Destroy(child.gameObject);
        }
        slots.Clear();
        items.Clear();
    }
}
