﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI.Extensions;

public class TGMSceneManager : MonoBehaviour {

    void Start()
    {
        Time.timeScale = 1;
    }

    public void loadScene(string scene)
    {
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }

    public void SelectLevel(string scene)
    {
        HorizontalScrollSnap levelsList = GameObject.Find("Horizontal Scroll Snap").GetComponent<HorizontalScrollSnap>();
        if (levelsList.CurrentPage == 0)
        {
            StartCoroutine(startLevel(scene));
        }
    }

    IEnumerator startLevel(string scene)
    {
        GameObject.Find("Canvas").transform.Find("Loading_window").gameObject.SetActive(true);
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }

}
