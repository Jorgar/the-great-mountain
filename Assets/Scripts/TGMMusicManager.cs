﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TGMMusicManager : MonoBehaviour {

    private bool isSound = true;

    public bool IsSound{get{ return isSound;}set{isSound = value;}}

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        if (!IsSound)
        {
            AudioListener.pause = true;
        }

        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(this.gameObject);
        }

    }

    void Update()
    {
        if (!IsSound)
        {
            AudioListener.pause = true;
        }
        else if (IsSound)
        {
            AudioListener.pause = false;
        }
    }

    
}
