﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TGMInputManager : MonoBehaviour {

    private bool isZooming;

    private Camera gameCamera;

    //Getters and setters
    public bool IsZooming{get{return isZooming; }set{isZooming = value;}}

    void Start()
    {
        gameCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        IsZooming = false;
    }

    void Update()
    {
        pinchToZoom();
    }

    void pinchToZoom()
    {
        if (Input.touchCount == 2 && !GameObject.Find("_Manager").GetComponent<TGMUiManager>().IsUIActivated)
        {
            IsZooming = true;

            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
           
            gameCamera.orthographicSize += deltaMagnitudeDiff * .05f;
            gameCamera.orthographicSize = Mathf.Clamp(gameCamera.orthographicSize, 5, 20);
        }
        else
        {
            IsZooming = false;
        }
    }
}
