﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceEffector : MonoBehaviour {

    private Rigidbody2D characterRigidBody2d;
    private SurfaceEffector2D iceSurfaceEffector;

	void Start () {
        characterRigidBody2d = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>();
        iceSurfaceEffector = this.gameObject.GetComponent<SurfaceEffector2D>();
	}
	
	void Update () {
        changeForeDirecction();
    }

    public void changeForeDirecction()
    {
        if(characterRigidBody2d.velocity.x < 0)
        {
            //Velocity of player is negative, set 
            //ice direction negative
            iceSurfaceEffector.speed =  -2;
        }else
        {
            //Velocity of player is positive and
            //ice direction is negative, set positive.
            iceSurfaceEffector.speed = 2; 
        }
    }
}
