﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UtilPost : MonoBehaviour {

    Animator animator;
    GameObject buttonUse;
    private TGMCharacterManager characterManager;
    private TGMUiManager uiManager;

    // Use this for initialization
    void Start () {
        animator = this.gameObject.transform.Find("Button_Use").gameObject.GetComponent<Animator>();
        buttonUse = this.gameObject.transform.Find("Button_Use").gameObject;
        characterManager = GameObject.FindGameObjectWithTag("Player").GetComponent<TGMCharacterManager>();
        uiManager = GameObject.Find("_Manager").GetComponent<TGMUiManager>();
    }
	
    void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.tag == "Player" && characterManager.State != TGMCharacterManager.Action.rapelling_horizontal)
        {
            buttonUse.SetActive(true);
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if(coll.gameObject.tag == "Player")
        {
            StartCoroutine(closeButton());
        }
    }

    IEnumerator closeButton()
    {
        animator.SetTrigger("Close_Use_button");
        yield return new WaitForSeconds(0.5f);
        buttonUse.SetActive(false);
    }

    public void HorizontalPressed()
    {
        if (GameObject.Find("Canvas").transform.Find("Inventory").gameObject.activeSelf)
        {
            StartCoroutine(uiManager.ActionInventory(false));
        }
        characterManager.HorizontalRapel();
        StartCoroutine(closeButton());
    }

    public void VerticalPressed()
    {
        characterManager.VerticalRapel();
        StartCoroutine(closeButton());
    }
}
