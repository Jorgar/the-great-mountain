﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UtilFlagTop : MonoBehaviour {

    GameObject flag;
    private TGMUiManager uiManager;


    void Start () {
        flag = this.gameObject.transform.Find("flagCanvas").gameObject.transform.Find("Button_Flag").gameObject;
        uiManager = GameObject.Find("_Manager").GetComponent<TGMUiManager>();
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.tag == "Player")
        {
            flag.SetActive(true);
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            StartCoroutine(CloseFlagButton());
        }
    }

    IEnumerator CloseFlagButton()
    {
        flag.GetComponent<Animator>().SetTrigger("Enter_Flag");
        yield return new WaitForSeconds(0.25f);
        flag.SetActive(false);
    }

    public void FlagPressed()
    {
        uiManager.WinTouch();
    }

}
