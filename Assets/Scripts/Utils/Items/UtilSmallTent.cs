﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UtilSmallTent : MonoBehaviour {

    public int secondsToSleep = 720;

    private GameObject buttonsItem;
    private TGMInventoryManager inventoryManager;
    private TGMDatabase database;
    private int secondsCounter;
    private TextMeshProUGUI textTime;
    private GameObject player;
    private TGMCharacterManager characterManager;

    void Start () {
        buttonsItem = this.gameObject.transform.Find("smallTentCanvas").transform.Find("menu").gameObject;
        inventoryManager = GameObject.Find("_Manager").GetComponent<TGMInventoryManager>();
        database = GameObject.Find("_Manager").GetComponent<TGMDatabase>();
        textTime = this.gameObject.transform.Find("smallTentCanvas").transform.Find("time").gameObject.GetComponent<TextMeshProUGUI>();
        secondsCounter = secondsToSleep;
        player = GameObject.FindWithTag("Player");
        characterManager = player.GetComponent<TGMCharacterManager>();
    }

    void OnTriggerEnter2D()
    {
        buttonsItem.SetActive(true);
    }

    void OnTriggerExit2D()
    {
        StartCoroutine(closeButtons());
    }

    IEnumerator closeButtons()
    {
        buttonsItem.GetComponent<Animator>().SetTrigger("UI_small_tent_close");
        yield return new WaitForSeconds(0.25f);
        buttonsItem.SetActive(false);
    }

    public void sleepButtonPressed()
    {
        StartCoroutine(closeButtons());
        characterManager.characterSleep(true);
        player.transform.position = this.transform.position;
        this.gameObject.transform.Find("sprite").gameObject.GetComponent<Animator>().SetTrigger("small_tent_sleep");
        this.gameObject.transform.Find("smallTentCanvas").transform.Find("time").gameObject.SetActive(true);
        characterManager.stopCalories();
        characterManager.stopSleep();
        characterManager.stopHidratation();

        StartCoroutine(timer());
    }

    public void pickButtonPressed()
    {
        List<Item> currentItems = new List<Item>();
        foreach (Item item in inventoryManager.Items)
        {
            if (item != null)
                currentItems.Add(item);
        }
        currentItems.Add(database.GetItem(this.name));
        inventoryManager.ClearInventory();
        inventoryManager.InitInventory();
        foreach (Item item in currentItems)
            inventoryManager.AddItem(item.ID);
        Destroy(this.gameObject);
    }

    string timeConversion(int seconds)
    {
        int minutes = seconds / 60;
        int secondsLeft = seconds % 60;
        if (minutes > 0)
        {
            return minutes + "m " + secondsLeft + "s";
        }
        else
        {
            return secondsLeft + "s";
        }
       
    }

    IEnumerator timer()
    {
        textTime.text = timeConversion(secondsCounter);
        yield return new WaitForSeconds(1);
        secondsCounter--;
        if (secondsCounter == 0)
        {
            characterManager.characterSleep(false);
            player.GetComponent<TGMCharacterManager>().AddSleep(Constants.MAX_VALUE_SLEEP);
            this.gameObject.transform.Find("smallTentCanvas").transform.Find("time").gameObject.SetActive(false);
            this.gameObject.transform.Find("sprite").gameObject.GetComponent<Animator>().SetTrigger("small_tent_sleep");
            player.transform.position = this.gameObject.transform.position;
            buttonsItem.SetActive(true);
            secondsCounter = secondsToSleep;
            characterManager.AddCalories((int)(0.30* characterManager.Calories)*-1);
            characterManager.AddHidratation((int)(0.30 * characterManager.Hidratation) * -1);
            characterManager.AddSleep((int)(0.30 * characterManager.Sleep) * -1);
            StartCoroutine(characterManager.DiscountCalories());
            StartCoroutine(characterManager.DiscountHidratation());
            StartCoroutine(characterManager.DiscountSleep());
        }
        else
        {
            StartCoroutine(timer());
        }
    }
}
