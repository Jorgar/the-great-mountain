﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemInteract : MonoBehaviour
{
    TGMUiManager uiManager;
    int itemIndex;

    void Start()
    {
        uiManager = GameObject.Find("_Manager").GetComponent<TGMUiManager>();
        itemIndex = Int32.Parse(this.name);
    }

    public void ItemPressed()
    {
        uiManager.ActualInventoryItemIndex = itemIndex;
        uiManager.ItemTouch();
    }
}
