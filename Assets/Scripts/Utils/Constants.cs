﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants : MonoBehaviour {

    //MAIN MENU


    //GAME
    public const int INVENTORY_PRESS = 0;
    public const int ITEM_PRESS = 1;
    public const int ITEM_INFO_PRESS = 2;
    public const int ITEM_USE_PRESS = 3;
    public const int ITEM_REMOVE_PRESS = 4;
    public const int ITEM_REMOVE_PRESS_YES = 5;
    public const int ITEM_REMOVE_PRESS_NO = 6;
    public const int INFO_CLOSE_PRESS = 7;

    public const int OPTIONS_PRESS = 8;
    public const int RESUME_PRESS = 9;
    public const int SOUND_PRESS = 10;
    public const int EXIT_PRESS = 11;
    public const int EXIT_YES_PRESS = 12;
    public const int EXIT_NO_PRESS = 13;
    public const int EXIT_CLOSE_PRESS = 14;

    public const int GAME_WIN = 15;
    public const int GAME_LOST = 16;


    //CHARACTER
    public const int MAX_VALUE_CALORIES = 3000;
    public const int MAX_VALUE_HIDRATATION = 1000;
    public const int MAX_VALUE_SLEEP = 100;

    public const float TIME_BETWEEN_CALORIES = 0.24f; //SECONDS
    public const float TIME_BETWEEN_HIDRATATION = 0.36f; //SECONDS
    public const float TIME_BETWEEN_SLEEP = 14.40f; //SECONDS

    //ITEMS
    public const string ITEM_BUILDING = "building";
    public const string ITEM_LIGHTNING = "lightning";
    public const string ITEM_CLIMBING = "climbing";

}
