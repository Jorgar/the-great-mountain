﻿using UnityEngine;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using GooglePlayGames.BasicApi;

public class GooglePlayManager : MonoBehaviour {

    #region PUBLIC_VAR
    private static string leaderboard = "CgkIpdivx-8HEAIQAQ";
    #endregion
    #region DEFAULT_UNITY_CALLBACKS

    // Use this for initialization
    void Start () {
        // recommended for debugging:
        PlayGamesPlatform.DebugLogEnabled = true;

        // Activate the Google Play Games platform
        PlayGamesPlatform.Activate();

        if (SceneManager.GetActiveScene().name.Equals("main_menu") && !Social.localUser.authenticated)
        {
            LogIn();
        }
    }

    #endregion
    #region BUTTON_CALLBACKS
    /// <summary>
    /// Login In Into Your Google+ Account
    /// </summary>
    public void LogIn()
    {
        Social.localUser.Authenticate((bool success) =>
        {
            if (success)
            {
                Debug.Log("Login Sucess");
            }
            else {
                Debug.Log("Login failed");
            }
        });
    }
    /// <summary>
    /// Shows All Available Leaderborad
    /// </summary>
    public void OnShowLeaderBoard()
    {
        PlayGamesPlatform.Instance.ShowLeaderboardUI(leaderboard);
    }
    /// <summary>
    /// Adds Score To leader board
    /// </summary>
    public void OnAddScoreToLeaderBoard()
    {
        long actualPuntuation = loadRecord();
        if (Social.localUser.authenticated)
        {
            Social.ReportScore(actualPuntuation + 1, leaderboard, (bool success) =>
            {
                if (success)
                {
                    Debug.Log("Update Score Success");
                }
                else {
                    Debug.Log("Update Score Fail");
                }
            });
        }
    }

    public long loadRecord()
    {
        long record = 0;
        PlayGamesPlatform.Instance.LoadScores(
        leaderboard,
        LeaderboardStart.PlayerCentered,
        1,
        LeaderboardCollection.Public,
        LeaderboardTimeSpan.AllTime, (LeaderboardScoreData data) => {
                   record = data.PlayerScore.value;
        });

        return record;
    }

    #endregion


}
