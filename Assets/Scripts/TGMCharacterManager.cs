﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TGMCharacterManager : MonoBehaviour {

    public float velocityPlayer = 5;

    public GameObject touch;
    public Text textUI;
    public Text textInsideBuilding;

    //Character characteristics
    [SerializeField]
    private int calories;
    [SerializeField]
    private int hidratation;
    [SerializeField]
    private int sleep;

    private Rigidbody2D characterRigidBody2d;
    private Transform characterTransform;
    private SpriteRenderer characaterSprite;
    private Vector2 positionToMove;
    private GameObject currentTouch;
    private bool inMovement;
    private Animator playerAnimator;

    private Action state;
    private bool insideClimbing;
    private bool insideRapelingHorizontal;
    private bool insideRapelingVertical;
    private bool insideWalkingEffort;
    private bool insideBuildingZone;

    private bool toMoveHorizontalRapel;
    private Vector2 centerHorizontalRapelPoint;
    private bool toMoveVerticalRapel;
    private Vector2 centerVerticalRapelPoint;

    private TGMUiManager uiManager;
    private bool isSleeping;

    private Coroutine caloriesCorutine;
    private Coroutine hidratationCoroutine;
    private Coroutine sleepCoroutine;


    //Getters and Setters
    public bool InsideWalkingEffort{get{return insideWalkingEffort;}set{insideWalkingEffort = value;}}
    public bool InsideRapelingVertical{get{return insideRapelingVertical;}set{insideRapelingVertical = value;}}
    public bool InsideRapelingHorizontal{get{return insideRapelingHorizontal;}set{insideRapelingHorizontal = value;}}
    public bool InsideClimbing{get{return insideClimbing;}set{insideClimbing = value;}}
    public Rigidbody2D CharacterRigidBody2d{get{return characterRigidBody2d;}set{characterRigidBody2d = value;}}
    public Vector2 PositionToMove{get{return positionToMove;}set{ positionToMove = value;}}
    public Transform CharacterTransform{get {return characterTransform;}set{characterTransform = value;}}
    public Action State{get{return state;}set{state = value;}}
    public bool InsideBuildingZone{get{return insideBuildingZone;}set{insideBuildingZone = value;}}
    public int Calories{get{return calories;}set{calories = value;}}
    public int Hidratation{get{ return hidratation;}set{ hidratation = value;}}
    public int Sleep {get{return sleep;}set {sleep = value;}}

    public enum Action //Action mode the character is actually doing
    {
        walking_normal,
        walking_effort,
        climbing,
        rapelling_horizontal,
        rapelling_vertical
    }

    void Start () {
        insideBuildingZone = false;
        isSleeping = false;
        CharacterRigidBody2d = this.gameObject.GetComponent<Rigidbody2D>();
        CharacterTransform = this.gameObject.GetComponent<Transform>();
        characaterSprite = this.gameObject.GetComponent<SpriteRenderer>();
        PositionToMove = CharacterTransform.position;
        playerAnimator = this.gameObject.GetComponent<Animator>();
        inMovement = false;
        State = Action.walking_normal;


        //UI
        uiManager = GameObject.Find("_Manager").GetComponent<TGMUiManager>();

        //Characterictics
        Calories = Constants.MAX_VALUE_CALORIES;
        Hidratation = Constants.MAX_VALUE_HIDRATATION;
        Sleep = Constants.MAX_VALUE_SLEEP;

        caloriesCorutine = StartCoroutine(DiscountCalories());
        hidratationCoroutine =  StartCoroutine(DiscountHidratation());
        sleepCoroutine =  StartCoroutine(DiscountSleep());
        StartCoroutine(CheckCharacteristics());
    }


    void Update()
    {
        textUI.text = "Calories: " + Calories + "\nHidratation: " + Hidratation + "\nSleep: " + Sleep;
        textInsideBuilding.text = "Building " + InsideBuildingZone + ", Sleeping" + isSleeping;
    }


    void FixedUpdate()
    {
        if (!uiManager.IsUIActivated)
            Movement();

        if (toMoveHorizontalRapel)
        {
            CharacterTransform.position = Vector2.MoveTowards(CharacterTransform.position, new Vector2(CharacterTransform.position.x, centerHorizontalRapelPoint.y+0.25f), velocityPlayer * Time.deltaTime);
            if (CharacterTransform.position.y == centerHorizontalRapelPoint.y + 0.25f)
            {
                toMoveHorizontalRapel = false;
                positionToMove = new Vector2(CharacterTransform.position.x + 3f, CharacterTransform.position.y);
            }
        }

        if (toMoveVerticalRapel)
        {
            CharacterTransform.position = Vector2.MoveTowards(CharacterTransform.position, new Vector2(centerVerticalRapelPoint.x, CharacterTransform.position.y), velocityPlayer * Time.deltaTime);
            if (CharacterTransform.position.x == centerVerticalRapelPoint.x)
            {
                toMoveVerticalRapel = false;
                positionToMove = new Vector2(CharacterTransform.position.x, CharacterTransform.position.y + 0.5f);
            }
        }
        Debug.Log(CharacterRigidBody2d.velocity);

        if (State == Action.walking_normal)
        {
            if (CharacterRigidBody2d.velocity.x > 0.2f)
            {
                CharacterRigidBody2d.bodyType = RigidbodyType2D.Dynamic;
            }
            else if (CharacterRigidBody2d.velocity.x < -0.2f && CharacterRigidBody2d.velocity.x >= -3.0f)
            {
                CharacterRigidBody2d.bodyType = RigidbodyType2D.Dynamic;
            }
            else
            {
                CharacterRigidBody2d.velocity = Vector2.zero;
                CharacterRigidBody2d.bodyType = RigidbodyType2D.Kinematic;
            }
        }
    }


    void Movement()
    {
        //Get the position to move
        if (Input.GetMouseButtonUp(0) && !GameObject.Find("_Manager").GetComponent<TGMInputManager>().IsZooming
            && GameObject.FindWithTag("Player").activeSelf && !isSleeping) //if click is not Zooming
        {
            PositionToMove = getMousePosition();
            //Debug.Log(positionToMove);
            currentTouch = Instantiate(touch, PositionToMove, Quaternion.identity);
            currentTouch.GetComponent<Renderer>().sortingLayerName = "UI";
            currentTouch.GetComponent<Renderer>().sortingOrder = -1;
            Destroy(currentTouch, 0.25f);
            inMovement = true;
            //Flip the character
            if (PositionToMove.x < CharacterTransform.position.x)
                flipCharacter(-1);  //left
            else
                flipCharacter(1);   //right
        }


        switch (State)
        {
            case Action.walking_normal:
                dimensionMov('h');
                break;
            case Action.climbing:
                dimensionMov('#');
                break;
            case Action.rapelling_horizontal:
                CharacterTransform.position = Vector2.MoveTowards(CharacterTransform.position, new Vector2(PositionToMove.x, CharacterTransform.position.y), velocityPlayer * Time.deltaTime);
                break;
            case Action.rapelling_vertical:
                CharacterTransform.position = Vector2.MoveTowards(CharacterTransform.position, new Vector2(CharacterTransform.position.x, PositionToMove.y), velocityPlayer * Time.deltaTime);
                break;
        }

    }


    void dimensionMov(char axis)
    {
        switch (axis)
        {
            case 'h':
                //horizontal x axis
                if ((PositionToMove.x + 1 < CharacterTransform.position.x) && (inMovement)) //right movement
                {
                    //CharacterRigidBody2d.bodyType = RigidbodyType2D.Dynamic;
                    CharacterRigidBody2d.velocity = new Vector2(-1 * velocityPlayer, CharacterRigidBody2d.velocity.y);
                    playerAnimator.SetBool("walking", true);
                }
                else if ((PositionToMove.x) - 1 > CharacterTransform.position.x && (inMovement)) //left movement
                {
                    //CharacterRigidBody2d.bodyType = RigidbodyType2D.Dynamic;
                    CharacterRigidBody2d.velocity = new Vector2(1 * velocityPlayer, CharacterRigidBody2d.velocity.y);
                    playerAnimator.SetBool("walking", true);
                }
                else
                {
                    //CharacterRigidBody2d.bodyType = RigidbodyType2D.Kinematic;
                    //rg.velocity = new Vector2(0,0);
                    playerAnimator.SetBool("walking", false);
                    inMovement = false;
                }
                break;

            case 'v':
                //vertical y axis
                if ((PositionToMove.y + 1 < CharacterTransform.position.y) && (inMovement))
                {
                    CharacterRigidBody2d.velocity = new Vector2(CharacterRigidBody2d.velocity.x, - 1 * velocityPlayer);
                }
                else if ((PositionToMove.y) - 1 > CharacterTransform.position.y && (inMovement))
                {
                    CharacterRigidBody2d.velocity = new Vector2(CharacterRigidBody2d.velocity.x, 1 * velocityPlayer);
                }
                else
                {
                    inMovement = false;
                }
                break;

            case '#':
                //vertical y and horizontal x axis.
                CharacterTransform.position = Vector2.MoveTowards((Vector2)CharacterTransform.position, PositionToMove, velocityPlayer*Time.deltaTime);
                break;
        }
    }


    void flipCharacter(float dir)
    {
        if (dir < 0)
        {
            characaterSprite.flipX = true;
        }
        else if (dir > 0)
        {
            characaterSprite.flipX = false;
        }
    }


    public Vector2 getMousePosition()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }


    void OnMouseDown()        //Use this method when click on the character collider. 
    {
        //Open or close the Inventory window
        if(!isSleeping && (!uiManager.IsUIActivated || uiManager.IsInventoryActivated)){
            uiManager.InventoryTouch();
        }
    }


    void OnTriggerEnter2D(Collider2D coll)
    {
        switch (coll.gameObject.tag)
        {
            case "climbing_wall":
                InsideClimbing = true;
                break;
            case "horizontal_rapel":
                InsideRapelingHorizontal = true;
                break;
            case "vertical_rapel":
                InsideRapelingVertical = true;
                break;
            case "building_zone":
                insideBuildingZone = true;
                break;
        }
    }

    void OnTriggerStay2D(Collider2D coll)
    {
        switch (coll.gameObject.tag)
        {
            case "building_zone":
                insideBuildingZone = true;
                break;
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        switch (coll.gameObject.tag)
        {
            case "climbing_wall":
                InsideClimbing = false;
                CharacterRigidBody2d.gravityScale = 1f;
                State = Action.walking_normal;
                playerAnimator.SetBool("climbing", false);
                break;
            case "horizontal_rapel":
                InsideRapelingHorizontal = false;
                CharacterRigidBody2d.gravityScale = 1f;
                State = Action.walking_normal;
                playerAnimator.SetBool("zip_line_normal", false);
                break;
            case "vertical_rapel":
                InsideRapelingVertical = false;
                State = Action.walking_normal;
                CharacterRigidBody2d.gravityScale = 1;
                playerAnimator.SetBool("zip_line_vertical", false);
                break;
            case "building_zone":
                insideBuildingZone = false;
                break;

        }
    }


    public void Climb()
    {
        if (InsideClimbing)
        {
            PositionToMove = new Vector2(CharacterTransform.position.x, CharacterTransform.position.y + 0.5f);
            CharacterRigidBody2d.bodyType = RigidbodyType2D.Dynamic;
            CharacterRigidBody2d.gravityScale = 0f;
            CharacterRigidBody2d.velocity = Vector2.zero;
            //CharacterTransform.position = new Vector2(CharacterTransform.position.x, CharacterTransform.position.y + 05f);
            State = Action.climbing;
            playerAnimator.SetBool("climbing", true);
        }
    }


    public void HorizontalRapel()
    {
        if (InsideRapelingHorizontal)
        {
            playerAnimator.SetBool("zip_line_normal", true);
            GameObject[] pointsToRapel = GameObject.FindGameObjectsWithTag("horizontal_rapel");
            float distancePoint = 99999999.9f;
            GameObject pointToMove = null;
            //Looking for the nearest point to the player to rapel 
            for (int i = 0; i < pointsToRapel.Length; i++)
            {
                float actualDistance = Vector2.Distance(CharacterTransform.position, pointsToRapel[i].transform.position);
                if (actualDistance < distancePoint)
                {
                    distancePoint = actualDistance;
                    pointToMove = pointsToRapel[i];
                }
            }
            CharacterRigidBody2d.gravityScale = 0f;
            centerHorizontalRapelPoint = pointToMove.transform.position;
            CharacterRigidBody2d.velocity = Vector2.zero;
            State = Action.rapelling_horizontal;
            toMoveHorizontalRapel = true;
        }
    }


    public void VerticalRapel()
    {
        if (InsideRapelingVertical)
        {
            GameObject[] pointsToRapel = GameObject.FindGameObjectsWithTag("vertical_rapel");
            float distancePoint = 99999999.9f;
            GameObject pointToMove = null;
            //Looking for the nearest point to the player to rapel 
            for (int i = 0; i < pointsToRapel.Length; i++)
            {
                float actualDistance = Vector2.Distance(CharacterTransform.position, pointsToRapel[i].transform.position);
                if (actualDistance < distancePoint)
                {
                    distancePoint = actualDistance;
                    pointToMove = pointsToRapel[i];
                }
            }
            CharacterRigidBody2d.gravityScale = 0f;
            centerVerticalRapelPoint = pointToMove.transform.position;
            CharacterRigidBody2d.velocity = Vector2.zero;
            State = Action.rapelling_vertical;
            toMoveVerticalRapel = true;
            playerAnimator.SetBool("zip_line_vertical", true);
        }
    }


    public void stopCharacter()
    {
        CharacterRigidBody2d.velocity = Vector2.zero;
        positionToMove = CharacterTransform.position;
    }


    public void AddCalories(int caloriesValue)
    {
        Calories = Mathf.Clamp(Calories + caloriesValue, 0, Constants.MAX_VALUE_CALORIES);
    }


    public void AddHidratation(int hidratationValue)
    {
        Hidratation = Mathf.Clamp(Hidratation + hidratationValue, 0, Constants.MAX_VALUE_HIDRATATION);
    }


    public void AddSleep(int sleepValue)
    {
        Sleep = Mathf.Clamp(Sleep + sleepValue, 0, Constants.MAX_VALUE_SLEEP);
    }

    public void characterSleep(bool dreaming)
    {
        SpriteRenderer characterRenderer = this.gameObject.GetComponent<SpriteRenderer>();
        if (dreaming)
        {
            isSleeping = true;
            characterRenderer.enabled = false;
        }
        else
        {
            isSleeping = false;
            characterRenderer.enabled = true;
        }
    }

    public void stopCalories()
    {
        StopCoroutine(caloriesCorutine);
    }

    public void stopHidratation()
    {
        StopCoroutine(hidratationCoroutine);
    }

    public void stopSleep()
    {
        StopCoroutine(sleepCoroutine);
    }

    public IEnumerator DiscountCalories()
    {
        yield return new WaitForSeconds(Constants.TIME_BETWEEN_CALORIES);
        Calories = Mathf.Clamp(Calories - 1, 0, Constants.MAX_VALUE_CALORIES);
        uiManager.FoodBar.value = Calories;
        caloriesCorutine = StartCoroutine(DiscountCalories());
    }


    public IEnumerator DiscountHidratation()
    {
        yield return new WaitForSeconds(Constants.TIME_BETWEEN_HIDRATATION);
        Hidratation = Mathf.Clamp(Hidratation - 1, 0, Constants.MAX_VALUE_HIDRATATION);
        uiManager.HidratationBar.value = Hidratation;
        hidratationCoroutine = StartCoroutine(DiscountHidratation());
    }


    public IEnumerator DiscountSleep()
    {
        yield return new WaitForSeconds(Constants.TIME_BETWEEN_SLEEP);
        Sleep = Mathf.Clamp(Sleep - 1, 0, Constants.MAX_VALUE_SLEEP);
        uiManager.SleepBar.value = Sleep;
        sleepCoroutine = StartCoroutine(DiscountSleep());
    }


    IEnumerator CheckCharacteristics()
    {
        GameObject emergentWindowSurvivor = GameObject.Find("playerCanvas").transform.Find("survivorEmergent").gameObject;
        if (Calories < ((int)(0.20f * Constants.MAX_VALUE_CALORIES)))
        {
            emergentWindowSurvivor.GetComponent<Image>().sprite = Resources.Load("Sprites/UI_elements/caloriesEmergent", typeof(Sprite)) as Sprite;
            emergentWindowSurvivor.SetActive(true);
            yield return new WaitForSeconds(8);
            emergentWindowSurvivor.GetComponent<Animator>().SetTrigger("UI_Emergent_survivor_close");
            yield return new WaitForSeconds(0.25f);
            emergentWindowSurvivor.SetActive(false);
        }

        if(Hidratation < ((int)(0.20f * Constants.MAX_VALUE_HIDRATATION)))
        {
            emergentWindowSurvivor.GetComponent<Image>().sprite = Resources.Load("Sprites/UI_elements/hidratationEmergent", typeof(Sprite)) as Sprite;
            emergentWindowSurvivor.SetActive(true);
            yield return new WaitForSeconds(8);
            emergentWindowSurvivor.GetComponent<Animator>().SetTrigger("UI_Emergent_survivor_close");
            yield return new WaitForSeconds(0.25f);
            emergentWindowSurvivor.SetActive(false);
        }

        if (Sleep < ((int)(0.20f * Constants.MAX_VALUE_SLEEP)))
        {
            emergentWindowSurvivor.GetComponent<Image>().sprite = Resources.Load("Sprites/UI_elements/sleepEmergent", typeof(Sprite)) as Sprite;
            emergentWindowSurvivor.SetActive(true);
            yield return new WaitForSeconds(8);
            emergentWindowSurvivor.GetComponent<Animator>().SetTrigger("UI_Emergent_survivor_close");
            yield return new WaitForSeconds(0.25f);
            emergentWindowSurvivor.SetActive(false);
        }

        yield return new WaitForSeconds(2.5f);

        if(Sleep == 0 || Hidratation == 0 || Calories == 0)
        {
            uiManager.GameOverTouch();
        }
        else
        {
            StartCoroutine(CheckCharacteristics());
        }
    }

}
