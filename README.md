![header.png](https://i.imgur.com/oPAsYDe.png)

# **DESCRIPTION** #

The Great Mountain is a videogame demo project made with Unity (v5.4.5 to 2018.1.0f2) by Jordi Garcia. It was developed for Android devices.

This project was created with academic purposes as final work of the Cross-platform Application Development Associate Degree. The whole project included the software, a project report and a speech. The final result was marked by the committee with 9 points over 10. Additional resources apart of this repository are the [project report](https://www.dropbox.com/s/nodjwybxba7qv3q/TFG_TGM.pdf?dl=0) in pdf and the [powerpoint used in the speech.](https://www.dropbox.com/s/9ond3xlh07n0zlo/2016-17.ProyectoDAMGarc%C3%ADa.pptx?dl=0)

# **LICENSE** #

Copyright 2018 Jordi Garcia. This project is published with [Apache 2.0 License](http://www.apache.org/licenses/LICENSE-2.0). Feel free to use everything on it (Scripts, sprites, etc) and continue this project as the same game or just using this idea and code to create your own.

# **PROJECT STRUCTURE** #

*Class diagram*

![classdiagram.png](https://i.imgur.com/kk0kb5W.png)


# **NOTES** #

If you have any question feel free to contact with me: jordigarciadev@gmail.com

Thank you for take a look to my repository. Cheers!